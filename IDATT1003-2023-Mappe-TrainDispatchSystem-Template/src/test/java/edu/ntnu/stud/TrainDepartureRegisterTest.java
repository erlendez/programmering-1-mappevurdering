package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * This class is responsible for testing the TrainDepartureRegister class.
 */
public class TrainDepartureRegisterTest {
  // Positive test cases.

  @Test
  public void testAddTrainDeparture() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDeparture departure1 = new TrainDeparture(LocalTime.now()
        .plusMinutes(1), 
        "L1", 
        "123", 
        "Oslo", 
        1, 
        LocalTime.of(0, 0));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.now()
        .plusMinutes(2), 
        "L2", 
        "456", 
        "Bergen", 
        2, 
        LocalTime.of(0, 0));
  
    register.addTrainDeparture(departure1);
    register.addTrainDeparture(departure2);
  
    assertTrue(register.getAllDeparturesSortedByTime().contains(departure1));
    assertTrue(register.getAllDeparturesSortedByTime().contains(departure2));
  }

  @Test
  public void testGetTrainDepartureByTrainNumber() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDeparture newDeparture = new TrainDeparture(LocalTime.now()
        .plusMinutes(1), 
        "L1", 
        "123", 
        "Oslo", 
        1, 
        LocalTime.of(0, 0));

    register.addTrainDeparture(newDeparture);

    assertNotNull(register.getTrainDepartureByTrainNumber(newDeparture.getTrainNumber()));
  }

  @Test
  public void testGetTrainDeparturesByDestination() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDeparture departure1 = new TrainDeparture(LocalTime.now()
        .plusMinutes(1), 
        "L1", 
        "123", 
        "Oslo", 
        1, 
        LocalTime.of(0, 0));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.now()
        .plusMinutes(2), 
        "L2", 
        "456", 
        "Bergen", 
        2, 
        LocalTime.of(0, 0));
  
    register.addTrainDeparture(departure1);
    register.addTrainDeparture(departure2);
  
    List<TrainDeparture> osloDepartures = register.getTrainDeparturesByDestination("Oslo");
    assertTrue(osloDepartures.contains(departure1));
    assertFalse(osloDepartures.contains(departure2));
  }

  @Test
  public void testRemoveDeparturesBeforeTime() {
    TrainDepartureRegister register = new TrainDepartureRegister();

    TrainDeparture departure1 = new TrainDeparture(LocalTime.now()
        .plusMinutes(1), "L1", "123", "Oslo", 1, LocalTime.of(0, 0));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.now()
        .plusMinutes(2), "L2", "456", "Bergen", 2, LocalTime.of(0, 15));

    register.addTrainDeparture(departure1);
    register.addTrainDeparture(departure2);

    LocalTime referenceTime = LocalTime.now().plusMinutes(30);

    register.removeDeparturesBeforeTime(referenceTime);

    assertFalse(register.getAllDeparturesSortedByTime().contains(departure1));
    assertFalse(register.getAllDeparturesSortedByTime().contains(departure2));
  }

  // Negative test cases.
  @Test
  public void testAddTrainDepartureAlreadyInRegister() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDeparture newDeparture1 = new TrainDeparture(LocalTime.now()
        .plusMinutes(1), "L1", "123", "Oslo", 1, LocalTime.of(0, 0));
    TrainDeparture newDeparture2 = new TrainDeparture(LocalTime.now()
        .plusMinutes(2), "L2", "123", "Bergen", 1, LocalTime.of(0, 0));
      
    register.addTrainDeparture(newDeparture1);

    assertThrows(IllegalArgumentException.class, () -> register.addTrainDeparture(newDeparture2));
  }

  @Test
  public void testRemoveDeparturesBeforeTimeWithInvalidTime() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    assertThrows(IllegalArgumentException.class, () -> register.removeDeparturesBeforeTime(null));
  }

  @Test
  public void testGetTrainDepartureByTrainNumberWithInvalidTrainNumber() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    assertThrows(IllegalArgumentException.class, () -> register
        .getTrainDepartureByTrainNumber("InvalidTrainNumber"));
  }

  @Test
  public void testGetTrainDeparturesByDestinationWithInvalidDestination() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    assertThrows(IllegalArgumentException.class, () -> register
        .getTrainDeparturesByDestination("InvalidDestination"));
  }

  @Test
  public void testRemoveNonExistentDeparturesBeforeTime() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    LocalTime time = LocalTime.now().plusMinutes(5);
    assertDoesNotThrow(() -> register.removeDeparturesBeforeTime(time));
  }

  @Test
  public void testGetNonExistentTrainDeparture() {
    TrainDepartureRegister register = new TrainDepartureRegister();

    assertThrows(IllegalArgumentException.class, () -> {
      register.getTrainDepartureByTrainNumber("NonExistentTrainNumber");
    });
  }

}