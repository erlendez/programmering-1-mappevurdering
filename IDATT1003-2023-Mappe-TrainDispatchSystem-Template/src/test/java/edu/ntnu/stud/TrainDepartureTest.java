package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import org.junit.jupiter.api.Test;

/**
 * This class contains unit tests for the TrainDeparture class.
 */
public class TrainDepartureTest {
    
  @Test
  public void testPositiveTrainDeparture() {
    LocalTime departureTime = LocalTime.now().plusMinutes(1);
    String line = "L1";
    String trainNumber = "123";
    String destination = "Oslo";
    int track = 1;
    LocalTime delay = LocalTime.of(0, 0);

    TrainDeparture trainDeparture = new TrainDeparture(
        departureTime, 
        line, 
        trainNumber, 
        destination, 
        track, 
        delay);

    assertEquals(departureTime, trainDeparture.getDepartureTime());
    assertEquals(line, trainDeparture.getLine());
    assertEquals(trainNumber, trainDeparture.getTrainNumber());
    assertEquals(destination, trainDeparture.getDestination());
    assertEquals(track, trainDeparture.getTrack());
    assertEquals(delay, trainDeparture.getDelay());
    assertFalse(trainDeparture.shouldRemove());
  }

  
  @Test
  public void testNegativeTrainDeparture() {
    String invalidDepartureTime = "25:00";
    String line = "L1";
    String trainNumber = "123";
    String destination = "Oslo";
    int track = 1;
    LocalTime delay = LocalTime.of(0, 0);

    assertThrows(DateTimeParseException.class, () -> {
      LocalTime departureTime = LocalTime.parse(invalidDepartureTime);
      new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);
    });
  }
}
