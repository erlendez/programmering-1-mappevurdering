package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * The TrainDispatchApp class is the main class for the train dispatch application.
 * It manages train departures, provides methods to interact with the TrainDepartureRegister,
 * and controls the overall flow of the train dispatch application.

 * @author Erlend Eide Zindel
 * @version 4.0
 * @since 2023-12-04
 */
public class TrainDispatchApp {

  // Fields

  /**
   * The TrainDepartureRegister instance that manages train departure information.
   */
  private TrainDepartureRegister register;
  
  /**
   * The current time in the application.
   */
  private LocalTime currentTime;

  // Methods

  /**
   * Initializes the application by creating a 
   * TrainDepartureRegister instance and setting the current time.
   */
  public void init() {
    this.register = new TrainDepartureRegister();
    this.currentTime = LocalTime.now();
  }

  /**
   * Starts the application by creating and adding initial train departures to the register,
   * then testing functionality by printing the information board.
   */
  public void start() {
      
    TrainDeparture departure1 = new TrainDeparture(LocalTime.now().plusMinutes(30), 
        "L1", 
        "101", 
        "Oslo", 
        1, 
        LocalTime.of(0, 0));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.now().plusMinutes(40), 
        "F4", 
        "202", 
        "Bergen", 
        2, 
        LocalTime.of(1, 0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.now().plusMinutes(50), 
        "L2", 
        "303", 
        "Trondheim", 
        3, 
        LocalTime.of(0, 5));

    register.addTrainDeparture(departure1);
    register.addTrainDeparture(departure2);
    register.addTrainDeparture(departure3);

    printTrainDepartureInfo();
  }

  /**
   * Gets the current time in the application.
   *
   * @return The current time.
   */

  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Prints the train departure information board to the console.
   */
  public void printTrainDepartureInfo() {
    List<TrainDeparture> departures = register.getAllDeparturesSortedByTime();
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    String formattedTime = currentTime.format(formatter);
    System.out.println(
        "\nTrain Departure Information Board: \n" 
        + "Current time: " + formattedTime + "\n");
    System.out.printf("%-15s%-10s%-10s%-15s%-15s%-10s\n",
        "Tidspunkt", "Linje", "Tognummer", "Destinasjon", "Forsinkelse", "Spor");
    for (TrainDeparture departure : departures) {
      System.out.println(departure); // Assuming TrainDeparture class has a toString() method.
    }
  }
  
  /**
   * Adds a new train departure to the TrainDepartureRegister.
  *
  * @param departureTime The time the train departs.
  * @param line          The train line or route.
  * @param trainNumber   The train number.
  * @param destination   The destination of the train.
  * @param track         The track the train departs from.
  * @param delay         The delay of the train.
  * @throws IllegalArgumentException If there is an issue adding the train departure.
  */
  public void addTrainDeparture(
      LocalTime departureTime, 
      String line, 
      String trainNumber, 
      String destination, 
      int track, 
      LocalTime delay
  ) throws IllegalArgumentException {
    TrainDeparture trainDeparture = new TrainDeparture(
        departureTime, 
        line, 
        trainNumber, 
        destination, 
        track, 
        delay);
    register.addTrainDeparture(trainDeparture);
  }

  /**
   * Gets all train departures sorted by departure time.
   *
   * @return A list of TrainDeparture objects sorted by departure time.
   */
  public List<TrainDeparture> getAllDeparturesSortedByTime() {
    return register.getAllDeparturesSortedByTime();
  }

  /**
   * Removes train departures from the TrainDepartureRegister before a given time.
   *
   * @param time The time before which train departures should be removed.
   */
  public void removeDeparturesBeforeTime(LocalTime time) {
    register.removeDeparturesBeforeTime(time);
  }

  /**
   * Searches for a train departure based on the train number.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture object with the specified train number, or null if not found.
   */
  public TrainDeparture getTrainDepartureByTrainNumber(String trainNumber) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    String formattedTime = currentTime.format(formatter);
    System.out.println("Current time: " + formattedTime + "\n");
    System.out.printf("%-15s%-10s%-10s%-15s%-15s%-10s\n",
            "Tidspunkt", "Linje", "Tognummer", "Destinasjon", "Forsinkelse", "Spor");
    return register.getTrainDepartureByTrainNumber(trainNumber);
  }

  /**
   * Searches for train departures based on a destination.
   *
   * @param destination The destination to search for.
   * @return A list of TrainDeparture objects with the specified destination.
   */
  public List<TrainDeparture> getTrainDeparturesByDestination(String destination) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    String formattedTime = currentTime.format(formatter);
    System.out.println("Current time: " + formattedTime + "\n");
    System.out.printf("%-15s%-10s%-10s%-15s%-15s%-10s\n",
            "Tidspunkt", "Linje", "Tognummer", "Destinasjon", "Forsinkelse", "Spor");
    return register.getTrainDeparturesByDestination(destination);
  }

  /**
   * Sets the track for a train departure based on the train number.
   *
   * @param trainNumber The train number.
   * @param track       The track to set.
   * @throws NoSuchElementException If no train departure is found with the given train number.
   */
  public void setTrackForTrainDepartureByTrainNumber(
      String trainNumber, 
      int track
  ) throws NoSuchElementException {
    Optional.ofNullable(register.getTrainDepartureByTrainNumber(trainNumber))
                  .ifPresentOrElse(trainDeparture -> {
                    trainDeparture.setTrack(track);
                  }, () -> {
                      throw new NoSuchElementException(
                        "Train departure not found with the given train number.");
                  });
  }

  /**
   * Adds delay for a train departure based on the train number.
   *
   * @param trainNumber The train number.
   * @param delay       The delay to add.
   * @throws NoSuchElementException If no train departure is found with the given train number.
   */
  public void addDelayForTrainDepartureByTrainNumber(
      String trainNumber, 
      LocalTime delay
  ) throws NoSuchElementException {
    Optional.ofNullable(register.getTrainDepartureByTrainNumber(trainNumber))
                  .ifPresentOrElse(trainDeparture -> {
                    trainDeparture.addDelay(delay);
                  }, () -> {
                      throw new NoSuchElementException(
                        "Train departure not found with the given train number.");
                  });
  }

  /**
   * Updates the clock to a new time and removes train departures based on the updated current time.
   *
   * @param newTime The new time to set.
   * @throws IllegalArgumentException If the new time is not later than the current time.
   */
  public void updateClock(LocalTime newTime) {
    if (newTime.isBefore(currentTime)) {
      throw new IllegalArgumentException("New time must be later than the current time");
    }

    this.currentTime = newTime;
    register.removeDeparturesBasedOnCurrentTime(this);
  }

  /**
   * The main method to start the train dispatch application.
   *
   * @param args Command-line arguments (not used in this application).
   */
  public static void main(String[] args) {
    TrainDispatchApp trainDispatchApp = new TrainDispatchApp();
    UserInterface ui = new UserInterface(trainDispatchApp);
    ui.init();
    ui.start();
  }
}