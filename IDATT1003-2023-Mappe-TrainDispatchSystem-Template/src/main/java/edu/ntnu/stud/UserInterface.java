package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The UserInterface class provides a simple command-line interface for 
 * the train dispatch application.
 * It allows users to interact with the TrainDispatchApp by providing 
 * menu options and handling user input.
 * The menu options include adding train departures, setting tracks, adding delays, 
 * searching for departures, updating the time, and displaying all departures.
 * The class is responsible for initiating and managing the train dispatch application.

 * @author Erlend Eide Zindel
 * @version 4.0
 * @since 2023-12-04
 */
public class UserInterface {
  private TrainDispatchApp trainDispatchApp;
  private Scanner scanner;
    
  /**
   * Constructs a UserInterface with the specified TrainDispatchApp.
   *
   * @param trainDispatchApp The TrainDispatchApp instance to be used by the user interface.
   */
  public UserInterface(TrainDispatchApp trainDispatchApp) {
    this.trainDispatchApp = new TrainDispatchApp();
    this.scanner = new Scanner(System.in);
  }
    
  /**
   * Initializes the user interface by invoking the init() method of the TrainDispatchApp.
   */
  public void init() {
    trainDispatchApp.init();
  }

  /**
   * Starts the user interface. Initiates the train dispatch application, displays the menu,
   * and handles user input until the user chooses to exit.
   */
  public void start() {
    trainDispatchApp.start();
    handleUserInput();
    scanner.close();
  }

  /**
   * Displays the menu options for the train dispatch system.
   */
  private void displayMenu() { 
    System.out.println("------ Train Dispatch System Menu ------");
    System.out.println("1. Add Train Departure");
    System.out.println("2. Set track for a Train Departure(search by Train Number)");
    System.out.println("3. Add delay for a Train Departure(search by Train Number)");
    System.out.println("4. Search for a Train Departure by Train Number");
    System.out.println("5. Search for a Train Departure by Destination");
    System.out.println("6. Update the time of day");
    System.out.println("7. Show All Departures");
    System.out.println("8. Exit");
  }

  /**
   * Handles user input by displaying the menu, accepting user choices,
   * and executing the corresponding actions until the user chooses to exit.
   */
  private void handleUserInput() {
    Scanner scanner = new Scanner(System.in);
    int choice;

    do {
      try {
        displayMenu();
        System.out.print("\nEnter your choice: ");
        choice = scanner.nextInt();
        scanner.nextLine(); // Consume the newline character

        switch (choice) {
          case 1:
            addTrainDeparture();
            break;
          case 2:
            setTrackForTrainDepartureByTrainNumber();
            break;
          case 3:
            addDelayForTrainDepartureByTrainNumber();
            break;
          case 4:
            searchTrainDepartureByTrainNumber();
            break;
          case 5:
            searchTrainDepartureByDestination();
            break;
          case 6:
            updateClock();
            break;
          case 7:
            showAllDepartures();
            break;
          case 8:
            System.out.println("Exiting...");
            break;
          default:
            System.out.println("Invalid choice. Please enter a valid option.");
        }
      } catch (InputMismatchException e) {
        System.out.println("Invalid input. Please enter a number.");
        scanner.nextLine(); // Clear the invalid input
        choice = 0;
      } catch (Exception e) {
        System.out.println("An error occurred: " + e.getMessage());
        choice = 0;
      }
    } while (choice != 8);

    scanner.close();
  }

  /**
   * Adds a new train departure by accepting user input for departure details.
   */
  private void addTrainDeparture() {
    try {
      System.out.print("Enter departure time (HH:mm): ");
      final LocalTime departureTime = LocalTime.parse(getUserInput(scanner));
      
      System.out.print("Enter line: ");
      final String line = getUserInput(scanner);
      
      System.out.print("Enter train number: ");
      final String trainNumber = getUserInput(scanner);
      
      System.out.print("Enter destination: ");
      final String destination = getUserInput(scanner);
      
      System.out.print("Enter track: ");
      int track = Integer.parseInt(getUserInput(scanner));
      
      System.out.print("Enter delay (HH:mm): ");
      LocalTime delay = LocalTime.parse(getUserInput(scanner));

      trainDispatchApp.addTrainDeparture(
          departureTime, 
          line, 
          trainNumber, 
          destination, 
          track, 
          delay);
      System.out.println("Train departure added successfully.");
    } catch (IllegalArgumentException e) {
      System.out.println("Error adding train departure: " + e.getMessage());
    }
  }

  /**
   * Sets the track for a train departure by accepting user input for train number and track.
   */
  public void setTrackForTrainDepartureByTrainNumber() {
    try {
      System.out.print("Enter train number: ");
      String trainNumber = scanner.next();

      System.out.print("Enter track: ");
      int track = scanner.nextInt();
      trainDispatchApp.setTrackForTrainDepartureByTrainNumber(trainNumber, track);
      System.out.println("Track set successfully.");
    } catch (NoSuchElementException e) {
      System.out.println("Error: " + e.getMessage());
    } catch (IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
      scanner.nextLine();
    }
  }

  /**
   * Adds a delay for a train departure by accepting user input for train number and delay.
   */
  public void addDelayForTrainDepartureByTrainNumber() {
    try {
      System.out.print("Enter train number: ");
      String trainNumber = scanner.next();

      System.out.print("Enter delay (HH:mm): ");
      String delayString = scanner.next();
      LocalTime delay = LocalTime.parse(delayString);

      trainDispatchApp.addDelayForTrainDepartureByTrainNumber(trainNumber, delay);
      System.out.println("Delay added successfully.");
    } catch (NoSuchElementException e) {
      System.out.println("Error: " + e.getMessage());
    } catch (IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
      scanner.nextLine();
    }
  }

  /**
   * Searches for a train departure by accepting user input for train number.
   */
  private void searchTrainDepartureByTrainNumber() {
    try {
      System.out.print("Enter the train number: ");
      String trainNumber = getUserInput(scanner);
      TrainDeparture trainDeparture = trainDispatchApp.getTrainDepartureByTrainNumber(trainNumber);
      if (trainDeparture != null) {
        System.out.println(trainDeparture);
      } else {
        System.out.println("Train Departure not found for the given train number.");
      }
    } catch (Exception e) {
      System.out.println("Error searching for train departure by train number: " + e.getMessage());
    }
  }

  /**
   * Searches for train departures by accepting user input for destination.
   */
  private void searchTrainDepartureByDestination() {
    try {
      System.out.print("Enter the destination: ");
      String destination = getUserInput(scanner);
      List<TrainDeparture> departures = trainDispatchApp
          .getTrainDeparturesByDestination(destination);
      

      if (!departures.isEmpty()) {
        departures.forEach(departure -> System.out.println(departure));
      } else {
        System.out.println("No Train Departures found for the given destination.");
      }
    } catch (Exception e) {
      System.out.println("Error searching for train departures by destination: " + e.getMessage());
    }
  }

  /**
   * Updates the time of day by accepting user input for the new time.
   */
  private void updateClock() {
    System.out.print("Enter the new time (HH:mm): ");
    String newTimeString = scanner.next();
  
    try {
      LocalTime parsedTime = LocalTime.parse(newTimeString);
      trainDispatchApp.updateClock(parsedTime);
      System.out.println("Time updated successfully.");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    } catch (DateTimeParseException e) {
      System.out.println("Error: Invalid time format.");
    }
  }

  /**
   * Displays all train departures using the TrainDispatchApp's printTrainDepartureInfo method.
   */
  private void showAllDepartures() {
    trainDispatchApp.printTrainDepartureInfo();
  }

  /**
   * Retrieves user input from the scanner.
   *
   * @param scanner The scanner to retrieve input from.
   * @return The user input as a String.
   */
  private String getUserInput(Scanner scanner) {
    return scanner.nextLine();
  }
}