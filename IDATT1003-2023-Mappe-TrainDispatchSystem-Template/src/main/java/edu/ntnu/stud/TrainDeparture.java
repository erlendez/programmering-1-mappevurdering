package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/** 
 * The TrainDeparture class represents information about a train departure.
 * It contains information about the departure time, 
 * line, train number, destination, track and delay.
 * "LocalTime" is used to represent time because it is a part of 
 * the Java 8 API and is a simple and effective way of handling time. 
 * "String" is used to represent the line, train number and destination 
 * because it gives simple representation and flexibility.
 * "int" is used to represent the track because it is a simple numerical value. 
 * This class ensures proper representation and handling of train departure data.

 * @author Erlend Eide Zindel
 * @version 4.0
 * @since 2023-12-04
*/
public class TrainDeparture {
  // Fields

  /**
   * The time the train departs.
   */
  private final LocalTime departureTime;

  /**
   * The train line or route (e.g., "L1", "F4", "R10").
   */
  private final String line;

  /**
   * The train number, unique for each train within a 24-hour period.
   */
  private final String trainNumber;

  /**
   * The destination of the train.
   */
  private final String destination;

  /**
   * The track the train departs from (-1 if no track is given).
   */
  private int track;

  /**
   * The delay of the train.
   */
  private LocalTime delay;

  /**
   * Constructor for creating a new train departure.
   *
   * @param departureTime The time the train departs.
   * @param line           The train line or route (e.g., "L1", "F4", "R10").
   * @param trainNumber    The train number, unique for each train within a 24-hour period.
   * @param destination    The destination of the train.
   * @param track          The track the train departs from (-1 if no track is given).
   * @param delay          The delay of the train.
   * @throws IllegalArgumentException If any of the parameters are invalid.
   */
  public TrainDeparture(
      LocalTime departureTime, 
      String line, 
      String trainNumber, 
      String destination, 
      int track, 
      LocalTime delay) { 

    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time cannot be null.");
    }

    if (line == null) {
      throw new IllegalArgumentException("Line cannot be null.");
    }

    if (trainNumber == null) {
      throw new IllegalArgumentException("Train number cannot be null.");
    }

    if (destination == null) {
      throw new IllegalArgumentException("Destination cannot be null.");
    }

    if (track < -1) {
      throw new IllegalArgumentException("Track cannot be less than -1.");
    }

    if (delay == null) {
      throw new IllegalArgumentException("Delay cannot be null.");
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  // Methods

  /**
   * Checks if the train departure should be removed based on the current time.
   *
   * @return True if the departure time is before the current time, false otherwise.
   */
  public boolean shouldRemove() {
    return departureTime.isBefore(LocalTime.now());
  }

  /**
   * Gets the time the train departs.
   *
   * @return The departure time.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the train line or route.
   *
   * @return The train line.
   */
  public String getLine() {
    return line;
  }

  /**
   * Gets the train number.
   *
   * @return The train number.
   */
  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Gets the destination of the train.
   *
   * @return The destination.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Gets the track the train departs from.
   *
   * @return The track.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Gets the delay of the train.
   *
   * @return The delay.
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets the track for the train departure.
   *
   * @param track The track to set.
   * @throws IllegalArgumentException If the track is less than -1.
   */
  public void setTrack(int track) {
    if (track >= -1) {
      this.track = track;
    } else {
      throw new IllegalArgumentException("Track cannot be less than -1");
    }
  }

  /**
   * Adds delay to the train departure.
   *
   * @param additionalDelay The additional delay to add.
   * @throws IllegalArgumentException If the additional delay is null or negative.
   */
  public void addDelay(LocalTime additionalDelay) {
    if (additionalDelay != null && !additionalDelay.isBefore(LocalTime.MIDNIGHT)) {
      delay = delay.plusHours(additionalDelay.getHour()).plusMinutes(additionalDelay.getMinute());
    } else {
      throw new IllegalArgumentException("Additional delay must be a non-negative LocalTime");
    }
  }

  /**
   * Represents the TrainDeparture object as a formatted String.
   *
   * @return A formatted string representing the TrainDeparture.
   */
  @Override
  public String toString() {
    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

    StringBuilder stringBuilder = new StringBuilder();

    if (track > 0) {
      stringBuilder.append(String.format("%-15s%-10s%-10s%-20s%-10s%-10s\n",
                departureTime.format(timeFormatter),
                line,
                trainNumber,
                destination,
                delay != null 
                && 
                !delay.equals(LocalTime.MIDNIGHT) ? delay.format(timeFormatter) : "",
                Integer.toString(track)));
    } else {
      stringBuilder.append(String.format("%-15s%-10s%-10s%-20s%-10s\n",
                departureTime.format(timeFormatter),
                line,
                trainNumber,
                destination,
                delay != null 
                && 
                !delay.equals(LocalTime.MIDNIGHT) ? delay.format(timeFormatter) : ""));
    }

    return stringBuilder.toString();
  }

}