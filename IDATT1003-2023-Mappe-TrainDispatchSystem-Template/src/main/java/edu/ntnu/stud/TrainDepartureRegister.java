package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
/**
 * The TrainDepartureRegister class manages a collection of TrainDeparture objects.
 * It provides methods to add, remove, and query train departures based on various criteria.
 * This class ensures proper management and retrieval of train departure information.

 * @author Erlend Eide Zindel
 * @version 4.0
 * @since 2023-12-04
 */
public class TrainDepartureRegister {
    
  // Fields

  /**
   * A map that associates train numbers with TrainDeparture objects.
   */
  private final HashMap<String, TrainDeparture> trainDepartureMap; 
  
  /**
   * Constructs a new TrainDepartureRegister with an empty map.
   */
  public TrainDepartureRegister() {
    this.trainDepartureMap = new HashMap<>();
  }
  
  // Methods

  /**
   * Adds a new train departure to the register.
   *
   * @param trainDeparture The TrainDeparture object to be added.
   * @throws IllegalArgumentException If the train departure 
   *      with the same train number already exists in the register.
   */
  public void addTrainDeparture(TrainDeparture trainDeparture) {
    String trainNumber = trainDeparture.getTrainNumber();
    if (!trainDepartureMap.containsKey(trainNumber)) {
      trainDepartureMap.put(trainNumber, trainDeparture);
    } else {
      throw new IllegalArgumentException("The Train Departure is already in the register.");
    }
  }

  /**
   * Removes train departures from the register before a given time.
   *
   * @param time The time before which train departures should be removed.
   */
  public void removeDeparturesBeforeTime(LocalTime time) {
    if (time == null) {
      throw new IllegalArgumentException("Invalid time: null");
    }
    trainDepartureMap.values().removeIf(departure -> 
        departure.getDepartureTime().isBefore(time) 
        || 
        departure.getDepartureTime()
        .plus(Duration.between(departure.getDepartureTime(), time)).isBefore(time)
    );
  }

  /**
  * Searches for a train departure based on the train number.
  *
  * @param trainNumber The train number to search for.
  * @return The TrainDeparture object with the specified train number.
  * @throws IllegalArgumentException If no train departure is found with the given train number.
  */
  public TrainDeparture getTrainDepartureByTrainNumber(String trainNumber) {
    if (!trainDepartureMap.containsKey(trainNumber)) {
      throw new IllegalArgumentException("Train departure not found with the given train number.");
    }
    return trainDepartureMap.get(trainNumber);
  }

  /**
  * Searches for train departures based on a destination.
  *
  * @param destination The destination to search for.
  * @return A list of TrainDeparture objects with the specified destination.
  * @throws IllegalArgumentException If no Train Departures are found for the given destination.
  */
  public List<TrainDeparture> getTrainDeparturesByDestination(String destination) {
    List<TrainDeparture> departures = trainDepartureMap.values().stream()
            .filter(departure -> departure.getDestination().equals(destination))
            .toList();

    if (departures.isEmpty()) {
      throw new IllegalArgumentException("No Train Departures found for the given destination.");
    }

    return departures;
  }
  
  /**
   * Returns all train departures sorted by departure time.
   *
   * @return A list of TrainDeparture objects sorted by departure time.
   */
  public List<TrainDeparture> getAllDeparturesSortedByTime() {
    return trainDepartureMap.values()
            .stream()
            .sorted(Comparator.comparing(departure -> departure.getDepartureTime())).toList();
  }

  /**
   * Removes train departures based on the current time, considering delays.
   *
   * @param trainDispatchApp The TrainDispatchApp providing the current time.
   */
  public void removeDeparturesBasedOnCurrentTime(TrainDispatchApp trainDispatchApp) {
    LocalTime currentTime = trainDispatchApp.getCurrentTime();

    trainDepartureMap.values().removeIf(departure -> {
      Duration delay = Duration.ofHours(departure.getDelay().getHour())
          .plusMinutes(departure.getDelay().getMinute());
      LocalDateTime departureDateTimeWithDelay = LocalDateTime.of(LocalDate.now(), 
          departure.getDepartureTime()).plus(delay);
        
      if (departureDateTimeWithDelay.toLocalTime().isBefore(departure.getDepartureTime())) {
        departureDateTimeWithDelay = departureDateTimeWithDelay.plusDays(1);
      }

      return departureDateTimeWithDelay.toLocalTime().isBefore(currentTime);
    });
  }
}